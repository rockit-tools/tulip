package net.brinkervii;

import lombok.extern.slf4j.Slf4j;
import net.brinkervii.tulip.InsaneConfigurationException;
import net.brinkervii.tulip.TulipCLIConfiguration;
import net.brinkervii.tulip.TulipTransformerEngine;
import picocli.CommandLine;
import picocli.CommandLine.Option;

import java.io.File;

@Slf4j
@CommandLine.Command(name = "TulipTransformer", mixinStandardHelpOptions = true, version = "Tulip Source Transformer 1.0")
public class TulipTransformer implements Runnable {
	@Option(names = {"-w", "--watch"})
	private boolean[] watch = new boolean[0];

	@Option(names = {"-d", "--directory"})
	private File[] outputDirectory;

	public static void main(String[] args) {
		CommandLine.run(new TulipTransformer(), args);
	}

	@Override
	public void run() {
		TulipCLIConfiguration configuration = new TulipCLIConfiguration();
		configuration.output(outputDirectory);
		configuration.watch(watch);

		try {
			configuration.checkSanity();
		} catch (InsaneConfigurationException e) {
			e.printStackTrace();
			return;
		}

		new TulipTransformerEngine(configuration).run();
	}
}
