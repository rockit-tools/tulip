package net.brinkervii.tulip;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;

import java.io.File;
import java.io.IOException;
import java.nio.file.FileVisitOption;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Set;
import java.util.stream.Collectors;

@Slf4j
public class TulipTransformerEngine implements Runnable {
	private final static ObjectMapper OBJECT_MAPPER = new ObjectMapper();

	private final TulipCLIConfiguration configuration;
	private final SourceBundles bundles = new SourceBundles();
	private TulipProjectConfiguration projectConfiguration;

	public TulipTransformerEngine(TulipCLIConfiguration configuration) {
		this.configuration = configuration;
	}

	@Override
	public void run() {
		log.info("Starting source transformer");

		log.info("Reading project configuration");
		final File projectConfigurationFile = new File("tulip-transform.json");
		if (!projectConfigurationFile.exists()) {
			log.error("Project configuration file was not found: " + projectConfigurationFile.toString());
			return;
		}

		try {
			this.projectConfiguration = OBJECT_MAPPER.readValue(projectConfigurationFile, TulipProjectConfiguration.class);
		} catch (IOException e) {
			e.printStackTrace();
			return;
		}

		if (bundleUp()) {
			final BundleTransformer transformer = new BundleTransformer(bundles.getAllFiles(), configuration.getOutputDirectory());
			transformer.run();
		} else {
			log.error("Failed to bundle up, cancelling source transformation");
		}

		log.info("Source transformer finished");
	}

	private boolean bundleUp() {
		bundles.clear();

		final Set<File> inputFiles = projectConfiguration.getSourcesRoot().stream()
				.map(File::new)
				.filter(File::exists)
				.filter(File::isDirectory)
				.collect(Collectors.toSet());

		inputFiles.stream()
				.map(inputFile -> {
					final SourceBundle bundle = new SourceBundle(inputFile);
					try {
						Files.walk(inputFile.toPath(), FileVisitOption.FOLLOW_LINKS)
								.filter(path -> path.toString().toLowerCase().endsWith(".lua"))
								.map(Path::toFile)
								.forEach(bundle::add);
					} catch (IOException e) {
						e.printStackTrace();
					}

					return bundle;
				})
				.forEach(bundles::add);

		return true;
	}
}
