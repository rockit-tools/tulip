package net.brinkervii.tulip;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;

public class FileUtil {
	public static File normalize(File file) {
		return file.toPath().toAbsolutePath().normalize().toFile();
	}

	public static File relativize(File relativeTo, File file) {
		final Path relativeToPath = Paths.get(relativeTo.getAbsolutePath()).normalize();
		final Path filePath = Paths.get(file.getAbsolutePath()).normalize();
		final Path relativePath = relativeToPath.relativize(filePath);

		return relativePath.toFile();
	}
}
