package net.brinkervii.tulip;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

import java.io.File;

@Slf4j
public class TulipCLIConfiguration {
	@Getter
	File outputDirectory = null;
	@Getter
	private boolean watching = false;

	public void output(File[] outputDirectory) {
		if (outputDirectory == null)
			return;

		for (File file : outputDirectory) {
			if (file.exists()) {
				if (file.isDirectory()) {
					this.outputDirectory = FileUtil.normalize(file);
				} else {
					log.warn(String.format("Output file '%s' is not a directory", file));
				}
			} else {
				log.warn(String.format("Output file '%s' does not exist", file));
			}
		}
	}

	public void checkSanity() throws InsaneConfigurationException {
		if (outputDirectory == null)
			throw new InsaneConfigurationException();

		if (!outputDirectory.exists())
			throw new InsaneConfigurationException();
	}

	public void watch(boolean[] watch) {
		for (boolean b : watch) {
			this.watching = this.watching || b;
		}
	}
}
