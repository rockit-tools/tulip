package net.brinkervii.tulip;

import lombok.extern.slf4j.Slf4j;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.*;

@Slf4j
public class BundleTransformer implements Runnable {
	private final List<SourceFile> sources;
	private final File outputRoot;

	public BundleTransformer(List<SourceFile> sources, File outputRoot) {
		this.sources = sources;
		this.outputRoot = outputRoot;
	}

	@Override
	public void run() {
		clean();

		final HashMap<String, SourceFile> sourceReference = new HashMap<>();
		sources.forEach(file -> sourceReference.put(file.getModuleName(), file));

		String ziptieSource;
		try {
			final InputStream resource = getClass().getClassLoader().getResourceAsStream("Ziptie.lua");
			if (resource == null) {
				log.error("Ziptie resource was null");
				return;
			}

			ziptieSource = new String(resource.readAllBytes());
		} catch (IOException e) {
			e.printStackTrace();
			return;
		}

		final byte[] ziptieSourceBytes = ziptieSource.getBytes(StandardCharsets.UTF_8);

		sources.stream()
				.filter(SourceFile::isEntryPoint)
				.map(file -> new EntryPointDependencies(sourceReference, file))
				.forEach(dependencies -> {
					final SourceFile entryPoint = dependencies.getEntryPoint();

					final var outputFile = this.write(entryPoint, sourceReference, entryPoint.outputFile(outputRoot).getParentFile());
					final var modulesFolder = new File(outputFile.getParentFile(), entryPoint.name() + "_Modules");
					dependencies.stream().forEach(file -> this.write(file, sourceReference, modulesFolder));

					try {
						Files.write(
								Paths.get(modulesFolder.toString(), "Ziptie.lua"),
								ziptieSourceBytes,
								StandardOpenOption.CREATE
						);
					} catch (IOException e) {
						e.printStackTrace();
					}
				});
	}

	private void clean() {
		Arrays.stream(Objects.requireNonNull(outputRoot.listFiles())).forEach(file -> {
			try {
				Files.walk(file.toPath())
						.sorted(Comparator.reverseOrder())
						.map(Path::toFile)
						.filter(f -> !f.isDirectory())
						.forEach(File::delete);
			} catch (IOException e) {
				e.printStackTrace();
			}
		});
	}

	private File write(SourceFile source, HashMap<String, SourceFile> sourceReference, File outputDirectory) {
		if (!outputDirectory.exists()) {
			outputDirectory.mkdirs();
		}

		File outputFile;
		if (source.isEntryPoint()) {
			final var spec = source.getEntryPointSpecification();
			if (spec.getLocation().equals(".")) {
				outputFile = new File(outputDirectory, source.getRelativeFile().getName());
			} else {
				outputFile = new File(outputRoot, spec.getLocation().replace(".", File.separator) + ".lua");
			}
		} else {
			outputFile = new File(outputDirectory, String.format("%s.lua", source.getModuleName()));
		}

		try {
			final var parentFile = outputFile.getParentFile();
			if (!parentFile.exists()) {
				parentFile.mkdirs();
			}

			Files.write(
					outputFile.toPath(),
					source.transform(sourceReference),
					StandardCharsets.UTF_8,
					StandardOpenOption.CREATE
			);
		} catch (IOException e) {
			e.printStackTrace();
		}

		return outputFile;
	}
}
