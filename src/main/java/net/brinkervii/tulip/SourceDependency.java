package net.brinkervii.tulip;

import lombok.Data;

@Data
public class SourceDependency {
	private final String variableName;
	private final String requireString;
	private final String trailer;

	public SourceDependency(String requireString, String trailer) {
		this.variableName = null;
		this.requireString = requireString;
		this.trailer = trailer;
	}
}
